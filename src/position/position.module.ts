import { Module } from '@nestjs/common';
import { PositionService } from './position.service';
import { PositionController } from './position.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Position } from '../models/position.model';
import { FileService } from '../file/file.service';

@Module({
  providers: [PositionService, FileService],
  controllers: [PositionController],
  imports: [SequelizeModule.forFeature([Position])],
})
export class PositionModule {}
