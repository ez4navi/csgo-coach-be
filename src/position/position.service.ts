import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Position } from '../models/position.model';
import { CreatePositionDto } from './dto/create-position.dto';
import { removeEmpty } from '../utils/rmEmptyItemsInObj';
import { FileService, FileType } from '../file/file.service';

@Injectable()
export class PositionService {
  constructor(
    @InjectModel(Position) private positionRepository: typeof Position,
    private fileService: FileService,
  ) {}
  async create(dto: CreatePositionDto, picture): Promise<Position> {
    const picturePath = this.fileService.createFile(FileType.IMAGE, picture);

    const position = await this.positionRepository.create({
      ...dto,
      picture: picturePath,
    });
    return position;
  }

  async getOne(id: number): Promise<Position> {
    return await this.positionRepository.findByPk(id);
  }

  async getAll(map_id: number, team: string): Promise<Position[]> {
    const params = {
      map_id,
      team,
    };

    const filteredParams = removeEmpty(params);

    return await this.positionRepository.findAll({
      where: {
        ...filteredParams,
      },
    });
  }

  async delete(id: number) {
    return await this.positionRepository.destroy({ where: { id } });
  }
}
