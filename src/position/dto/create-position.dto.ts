export class CreatePositionDto {
  readonly title: string;
  readonly team: string;
  readonly tiktok_link: string;
  readonly map_id: number;
}
