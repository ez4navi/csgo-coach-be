import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { PositionService } from './position.service';
import { CreatePositionDto } from './dto/create-position.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('/positions')
export class PositionController {
  constructor(private positionService: PositionService) {}

  @Post()
  @UseInterceptors(FileInterceptor('picture'))
  create(
    @Body() positionDto: CreatePositionDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return this.positionService.create(positionDto, file);
  }

  @Get(':id')
  getOne(@Param('id') id: number) {
    return this.positionService.getOne(id);
  }

  @Get()
  getAll(@Query('map_id') map_id: number, @Query('team') team: string) {
    return this.positionService.getAll(map_id, team);
  }

  @Delete(':id')
  delete(@Param('id') id: number) {
    return this.positionService.delete(id);
  }
}
