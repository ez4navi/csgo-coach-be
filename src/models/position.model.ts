import {
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { Map } from './map.model';

interface PositionCreationAttrs {
  title: string;
  picture: string;
  team: string;
  tiktok_link: string;
  map_id: number;
}

@Table({ tableName: 'positions' })
export class Position extends Model<Position, PositionCreationAttrs> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column({ type: DataType.STRING })
  title: string;

  @Column({ type: DataType.STRING })
  team: string;

  @Column({ type: DataType.STRING })
  picture: string;

  @Column({ type: DataType.STRING })
  tiktok_link: string;

  @ForeignKey(() => Map)
  @Column({ type: DataType.INTEGER })
  map_id: number;
}
