import { Column, DataType, Model, Table, HasMany } from 'sequelize-typescript';
import { Position } from './position.model';

interface MapCreationAttrs {
  title: string;
  picture: string;
}

@Table({ tableName: 'maps' })
export class Map extends Model<Map, MapCreationAttrs> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column({ type: DataType.STRING })
  title: string;

  @Column({ type: DataType.STRING })
  picture: string;

  @HasMany(() => Position)
  positions: Position[];
}
