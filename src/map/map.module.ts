import { Module } from '@nestjs/common';
import { MapService } from './map.service';
import { MapController } from './map.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Map } from '../models/map.model';
import { FileService } from '../file/file.service';

@Module({
  providers: [MapService, FileService],
  controllers: [MapController],
  imports: [SequelizeModule.forFeature([Map])],
})
export class MapModule {}
