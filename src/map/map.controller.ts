import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { MapService } from './map.service';
import { CreateMapDto } from './dto/create-map.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('/maps')
export class MapController {
  constructor(private mapService: MapService) {}

  @Post()
  @UseInterceptors(FileInterceptor('picture'))
  create(
    @Body() mapDto: CreateMapDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return this.mapService.create(mapDto, file);
  }

  @Get()
  getAll() {
    return this.mapService.getAll();
  }

  @Get(':id')
  getOne(@Param('id') id: number) {
    return this.mapService.getOne(id);
  }

  @Delete(':id')
  delete(@Param('id') id: number) {
    return this.mapService.delete(id);
  }
}
