import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Map } from '../models/map.model';
import { CreateMapDto } from './dto/create-map.dto';
import { FileService, FileType } from '../file/file.service';

@Injectable()
export class MapService {
  constructor(
    @InjectModel(Map) private mapRepository: typeof Map,
    private fileService: FileService,
  ) {}

  async create(dto: CreateMapDto, picture): Promise<Map> {
    const picturePath = this.fileService.createFile(FileType.IMAGE, picture);

    const map = await this.mapRepository.create({
      ...dto,
      picture: picturePath,
    });
    return map;
  }

  async getAll(): Promise<Map[]> {
    const maps = await this.mapRepository.findAll();
    return maps;
  }

  async getOne(id: number): Promise<Map> {
    return await this.mapRepository.findByPk(id, { include: { all: true } });
  }

  async delete(id: number) {
    return await this.mapRepository.destroy({ where: { id } });
  }
}
